import { useEffect, useState } from 'react';
import './App.css';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import SearchIcon from "@mui/icons-material/Search";
import CircularProgress from '@mui/material/CircularProgress';
import CancelIcon from '@mui/icons-material/Cancel';

function App() {
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [creationTime, setCreationTime] = useState('');
  const [blogImage, setBlogImage] = useState('');
  const [description, setDescription] = useState('');
  const [showForm, setShowForm] = useState(false);
  const [emptyInput, setEmptyInput] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [correctInput, setCorrectInput] = useState(false);
  const [articles, setArticles] = useState([]);
  const [term, setTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [active, setActive] = useState(false);
  const [btnType, setBtnType] = useState(undefined);
  const [loading, setLoading] = useState(false);

  const getSearchTerm = (e) => {
    console.log('e', e)
    setTerm(e)
  }

  // Search and filter articles
  useEffect(() => {
    console.log(term);
    if(term === undefined) {
      return setSearchResults(articles)
    }

    if(term !== "") {
      var newArticleList = articles.filter((article) => {
        return Object.values(article).join(" ").toLowerCase().includes(term.toLowerCase())
      })
      setSearchResults(newArticleList)
    } else {
      setSearchResults(articles)
    }
  }, [term])


  // Set creation time and initialize local storage
  useEffect(() => {
    setCreationTime(new Date().toLocaleDateString())
    if(!localStorage.getItem('articles')) {
      localStorage.setItem('articles', JSON.stringify([]))
    }

  }, [creationTime])


  // Fetch stored articles from local storage 
  // useEffect(() => {
  //   if(articles.length > 0) {
  //     let newArticles = JSON.parse(localStorage.getItem('articles') || '[]')
  //     setArticles([articles, ...newArticles])
  //   }
  // }, [])


  // Convert articles from string to objects
  // Pass stored articles to article state
  // Fetch stored articles from local storage 
  useEffect(() => {
    let newArticleList = JSON.parse(localStorage.getItem('articles'))
  
    setLoading(true);

    setTimeout(() => {
      setLoading(false)
      setArticles(newArticleList.reverse())
    }, 2000);
  }, [])


  // Cancel form submission 
  const handleCancel = () => {
    setShowForm(false);
    setErrorMessage(false);
    setEmptyInput(false);
    setTitle('');
    setCategory('');
    setCreationTime('');
    setBlogImage('');
    setDescription('')
  }

  // Create a new article 
  // Sumbit Form
  const handleSubmit = (e) => {
    e.preventDefault();

    if(!title || !category || !blogImage || !creationTime || !description) {
      return (
        setEmptyInput(true),
        setErrorMessage(true)
      )
    }

    const newArticle = {
      title, 
      category, 
      blogImage,
      creationTime, 
      description
    }

    let newArticles = JSON.parse((localStorage.getItem('articles')))
    
    newArticles.push(newArticle)

    localStorage.setItem('articles', JSON.stringify(newArticles))

    newArticle && setArticles([newArticle, ...articles])

    handleCancel();
  }

  // Filter articles by filter buttons
  const handleFilter = (type) => {
    type ? setActive(true) : setActive(false)
    type && setBtnType(type)

    getSearchTerm(type)  
  }

  // Cancel filter
  // Return to normal articles
  const cancelFilter = () => {
    setActive(false);
    setTerm("");
  }


  return (
    <div className="app">
      <header>
        <h1>Blog Application</h1>
        <div className='search'>
          <input type="text" placeholder='Search' 
            onChange={(e) => getSearchTerm(e.target.value)}/>
          <SearchIcon 
            className='icon' 
          />
        </div>
        <div className='create'>
          <p onClick={() => setShowForm(!showForm)}><span>+</span>Create an article</p>
        </div>
      </header>

      <div className='mainContainer'>
        <div className='filterBtns'>
          <button 
            name='tech' 
            onClick={(e) => handleFilter(e.target.name)}
            className={`${btnType === 'tech' && active && 'activeBtn'}`}
            disabled={articles.length < 1}
            >
              Tech
              {btnType === 'tech' && active && <CancelIcon className='cancelIcon' onClick={() => cancelFilter()}/>}
            </button>
          <button 
            name='entertainment' 
            onClick={(e) => handleFilter(e.target.name)}
            className={`${btnType === 'entertainment' &&  active && 'activeBtn'}`}
            disabled={articles.length < 1}
            >
              Entertainment
              {btnType === 'entertainment' && active && <CancelIcon className='cancelIcon' onClick={() => cancelFilter()}/>}
            </button>
          <button 
            name='community' 
            onClick={(e) => handleFilter(e.target.name)}
            className={`${btnType === 'community' && active && 'activeBtn'}`}
            disabled={articles.length < 1}
            >
              Community
              {btnType === 'community' && active && <CancelIcon className='cancelIcon' onClick={() => cancelFilter()}/>}
            </button>
        </div>
        {loading ?
        <div style={{display: 'flex', justifyContent: 'center'}}>
          <CircularProgress />
        </div>
        :
        <div className='articles'>
          {
            term?.length < 1 ? 
            (
              articles.length < 1 ?
              <h1 style={{textAlign: 'center', width: '100%'}}>No article found</h1>
              :
              articles?.map((article, key) => (
                <div className='article' key={key}>
                  <div className='articleHead'>
                    <img src={article.blogImage} alt="article" />
                    <p>{article.category}</p>
                  </div>
                  <div className='articleBody'>
                    <h3 className='articleTitle'>{article.title}</h3>
                    <div>{article.description}</div>   
                    <div className='date'>
                      {article.creationTime}  
                    </div>  
                  </div>      
                </div>
              ))
            )
            :
            (
              searchResults.length < 1 ?
              <h1 style={{textAlign: 'center', width: '100%'}}>No article found</h1>
              :
              searchResults?.map((article, key) => (
                <div className='article' key={key}>
                  <div className='articleHead'>
                    <img src={article.blogImage} alt="article" />
                    <p>{article.category}</p>
                  </div>
                  <div className='articleBody'>
                    <h3 className='articleTitle'>{article.title}</h3>
                    <div>{article.description}</div>   
                    <div className='date'>
                      {article.creationTime}  
                    </div>  
                  </div>      
                </div>
              ))              
            )

          }
        </div>
        }
      </div>

      {showForm && (
      <div className='form'>
        <form onSubmit={handleSubmit}>
          <div className='formGroup'>
            <label>Title</label>
            <input 
              type="text"
              value={title}
              onChange={(e) => {
                setTitle(e.target.value)
              }}
              className={`${!title && emptyInput && 'emptyInput'} ${title.length > 0 && correctInput && 'correctInput'}`}
            />
            {!title && errorMessage && <p className='errorMessage'>Title is required</p> }
          </div>

          <div className='formGroup'>
            <label>Category</label>
            <Select 
              value={category} 
              onChange={(e) => {
                setCategory(e.target.value)
                setCorrectInput(true)
              }}
              sx={{
                height: 40,
                fontSize: '.9rem'
              }}
              className={`select ${!category && emptyInput && 'emptyInput'} ${category.length > 0 && correctInput && 'correctInput'}`}
            >
              <MenuItem value='tech'>Tech</MenuItem>
              <MenuItem value='entertainment'>Entertainment</MenuItem>
              <MenuItem value='community'>Community</MenuItem>
              
            </Select>

              {!category && errorMessage && <p className='errorMessage'>Category is required</p> }
          </div>

          <div className='formGroup'>
            <label>Blog Image <b>(URL)</b></label>
            <input 
              type="text" 
              value={blogImage}
              onChange={(e) => {
                setBlogImage(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!blogImage && emptyInput && 'emptyInput'} ${blogImage.length > 0 && correctInput && 'correctInput'}`}
            />
            {!blogImage && errorMessage && <p className='errorMessage'>Blog image is required</p> }

          </div>
          <div className='formGroup'>
            <label>Creation Time</label>
            <input 
              type="text"
              value={creationTime}
              onChange={(e) => {
                setCorrectInput(true)
              }}
              className={`${!creationTime && emptyInput && 'emptyInput'} ${creationTime.length > 0 && correctInput && 'correctInput'}`}
            />
            {!creationTime && errorMessage && <p className='errorMessage'>Creation time is required</p> }
          </div>

          <div className='formGroup'>
            <label>Description</label>
            <textarea 
              value={description}
              onChange={(e) => {
                setDescription(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!description && emptyInput && 'emptyInput'} ${description.length > 0 && correctInput && 'correctInput'}`}
            />
            {!description && errorMessage && <p className='errorMessage'>Description is required</p> }

          </div>
          <div className='btns'>
            <button className='saveBtn'>Create article</button>
            <button className='cancelBtn' onClick={handleCancel}>Cancel</button>
          </div>
        </form>
      </div>
      )}
    </div>
  );
}

export default App;
